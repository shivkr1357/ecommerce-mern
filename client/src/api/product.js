import axios from 'axios';

export const createProduct = async (formData) => {
   const config = {
      headers: {
         'Content-Type': 'multipart/form-data',
      },
   };

   const response = await axios.post('/api/products/', formData, config);

   return response;
};
