import { STOP_LOADING, START_LOADING } from '../constants/loadingConstants';
import {
   SHOW_ERROR_MESSAGE,
   SHOW_SUCCESS_MESSAGE,
} from '../constants/messageConstants';
import { CREATE_PRODUCT, GET_PRODUCTS } from '../constants/productsConstants';
import axios from 'axios';

export const getProducts = () => async (dispatch) => {
   try {
      dispatch({ type: START_LOADING });
      const response = await axios.get('/api/products/');
      dispatch({ type: STOP_LOADING });
      dispatch({ type: GET_PRODUCTS, payload: response.data.products });
   } catch (err) {
      dispatch({ type: STOP_LOADING });
      console.log('Get Product Error = ', err);
      dispatch({
         type: SHOW_ERROR_MESSAGE,
         payload: err.response.data.errorMessage,
      });
   }
};

export const createProduct = (formData) => async (dispatch) => {
   try {
      const config = {
         headers: {
            'Content-Type': 'multipart/form-data',
         },
      };
      dispatch({ type: START_LOADING });
      const response = await axios.post('/api/products/', formData, config);
      dispatch({ type: STOP_LOADING });
      dispatch({
         type: SHOW_SUCCESS_MESSAGE,
         payload: response.data.successMessage,
      });
      dispatch({
         type: CREATE_PRODUCT,
         payload: response.data.product,
      });
   } catch (err) {
      console.log('createProduct Error', err);
      dispatch({ type: STOP_LOADING });
      dispatch({
         type: SHOW_ERROR_MESSAGE,
         payload: err.response.data.errorMessage,
      });
   }
};
