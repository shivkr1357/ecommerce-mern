import React, { Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';
import './header.css';
import { isAuthinticated, logout } from '../Helpers/auth';

const Header = ({ history }) => {
   const handleLogout = (evt) => {
      logout(() => {
         history.push('/signIn');
      });
   };

   const showNavigation = () => (
      <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
         <button
            className='navbar-toggler'
            type='button'
            data-toggle='collapse'
            data-target='#navbarTogglerDemo01'
            aria-controls='navbarTogglerDemo01'
            aria-expanded='false'
            aria-label='Toggle navigation'
         >
            <span className='navbar-toggler-icon'></span>
         </button>
         <div className='collapse navbar-collapse' id='navbarTogglerDemo01'>
            <Link to='/' className='navbar-brand'>
               Pet Food
            </Link>
            <ul className='navbar-nav ml-auto mt-2 mt-lg-0'>
               {!isAuthinticated() && (
                  <Fragment>
                     <li className='nav-item'>
                        <Link to='/' className='nav-link'>
                           <i className='fas fa-home'></i> Home
                        </Link>
                     </li>
                     <li className='nav-item'>
                        <Link to='/signUp' className='nav-link'>
                           <i className='fas fa-edit'></i> SignUp
                        </Link>
                     </li>
                     <li className='nav-item'>
                        <Link to='/signIn' className='nav-link'>
                           <i className='fas fa-sign-in-alt'></i> SignIn
                        </Link>
                     </li>
                  </Fragment>
               )}

               {isAuthinticated() && isAuthinticated().role === 0 && (
                  <Fragment>
                     <li className='nav-item'>
                        <Link to='/user/dashboard' className='nav-link'>
                           <i className='fas fa-home'></i> Dashboard
                        </Link>
                     </li>
                  </Fragment>
               )}

               {isAuthinticated() && isAuthinticated().role === 1 && (
                  <Fragment>
                     <li className='nav-item'>
                        <Link to='/admin/dashboard' className='nav-link'>
                           <i className='fas fa-home'></i> Dashboard
                        </Link>
                     </li>
                  </Fragment>
               )}

               {isAuthinticated() && (
                  <Fragment>
                     <li className='nav-item'>
                        <button className='btn btn-info' onClick={handleLogout}>
                           <i className='fas fa-sign-out-alt'></i> Logout
                        </button>
                     </li>
                  </Fragment>
               )}
            </ul>
            {/* <form className="form-inline my-2 my-lg-0">
                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form> */}
         </div>
      </nav>
   );

   return <header id='header'>{showNavigation()}</header>;
};

export default withRouter(Header);
