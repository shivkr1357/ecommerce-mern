import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './Header/Header';
import Home from './Home/Home';
import SignIn from './SignIn/SignIn';
import SignUp from './SignUp/SignUp';
import AdminDashboard from './AdminDashboard/AdminDashboard';
import UserDashboard from './UserDashboard/UserDashboard';
import AdminRoutes from './AdminRoutes/AdminRoutes';
import NotFound from './NotFound/NotFound';
import UserRoutes from './UserRoutes/UserRoutes';

const App = () => {
   return (
      <BrowserRouter>
         <Header />
         <main>
            <Switch>
               <Route exact path='/' component={Home} />
               <Route exact path='/signUp' component={SignUp} />
               <Route exact path='/signIn' component={SignIn} />
               <UserRoutes
                  exact
                  path='/user/dashboard'
                  component={UserDashboard}
               />
               <AdminRoutes
                  exact
                  path='/admin/dashboard'
                  component={AdminDashboard}
               />
               <Route component={NotFound} />
            </Switch>
         </main>
      </BrowserRouter>
   );
};

export default App;
