import React, { useState, Fragment } from 'react';
import { showErrMsg, showSuccessMsg } from '../Helpers/messages';
import { showLoading } from '../Helpers/loading';
import isEmpty from 'validator/lib/isEmpty';

// redux

import { useSelector, useDispatch } from 'react-redux';
import { clearMessages } from '../../Redux/actions/messageActions';
import { createCategory } from '../../Redux/actions/categoryActions';

const AdminCategoryModal = () => {
   /*****************************
    *
    *  REDUX GLOBAL STATE PROPERTIES
    *
    *  ************************/
   const { successMsg, errorMsg } = useSelector((state) => state.messages);
   const { loading } = useSelector((state) => state.loading);

   const dispatch = useDispatch();
   /*****************************
    *
    *  COMPONENT STATE PROPERTIES
    *
    *  ************************/

   const [category, setCategory] = useState('');
   const [clientSideErrorMsg, setclientSideErrorMsg] = useState('');

   /** EVENT  HANDLERS  */

   const handleCategoryChange = (e) => {
      dispatch(clearMessages());
      setCategory(e.target.value);
   };
   const handleMessage = () => {
      dispatch(clearMessages());
   };

   const handleCategorySubmit = (evt) => {
      evt.preventDefault();

      if (isEmpty(category)) {
         setclientSideErrorMsg('Please Enter a categroy');
      } else {
         showLoading(loading);
         const data = { category };
         dispatch(createCategory(data));
         setCategory('');
      }
   };

   return (
      /**  VIEWS  */
      <div id='addCategoryModal' className='modal' onClick={handleMessage}>
         <div className='modal-dialog modal-dialog-centered modal-lg'>
            <div className='modal-content'>
               <form onSubmit={handleCategorySubmit}>
                  <div className='modal-header bg-info text-white'>
                     <h5 className='modal-title'>Add Category</h5>
                     <button className='close' data-dismiss='modal'>
                        <i className='fas fa-times'></i>
                     </button>
                  </div>
                  <div className='modal-body my-2'>
                     {clientSideErrorMsg && showErrMsg(clientSideErrorMsg)}
                     {errorMsg && showErrMsg(errorMsg)}
                     {successMsg && showSuccessMsg(successMsg)}
                     {loading ? (
                        <div className='text-center'>
                           {showLoading(loading)}
                        </div>
                     ) : (
                        <Fragment>
                           <label htmlFor='category_name'>Category Name</label>
                           <input
                              type='text'
                              className='form-control'
                              value={category}
                              onChange={handleCategoryChange}
                           />
                        </Fragment>
                     )}
                  </div>
                  <div className='modal-footer'>
                     <button data-dismiss='modal' className='btn btn-secondary'>
                        Close
                     </button>
                     <button type='submit' className='btn btn-primary'>
                        Submit
                     </button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   );
};

export default AdminCategoryModal;
