import React, { useEffect } from 'react';
import './admin.css';
/****  COMPONENTS   *****/

import AdminActionBtns from './AdminActionBtns';
import AdminCategoryModal from './AdminCategoryModal';
import AdminProductModal from './AdminProductModal';
import AdminBody from './AdminBody';

//  redux

import { useDispatch } from 'react-redux';
import { getCategories } from '../../Redux/actions/categoryActions';
import { getProducts } from '../../Redux/actions/productActions';

const AdminDashboard = () => {
   const dispatch = useDispatch();

   useEffect(() => {
      dispatch(getCategories());
   }, [dispatch]);

   useEffect(() => {
      dispatch(getProducts());
   }, [dispatch]);
   /*********** RENDERER ************/
   return (
      <section>
         <AdminActionBtns />
         <AdminCategoryModal />
         <AdminProductModal />
         <AdminBody />
      </section>
   );
};

export default AdminDashboard;
