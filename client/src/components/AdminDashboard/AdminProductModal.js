import React, { useState, Fragment } from 'react';
import { showErrMsg, showSuccessMsg } from '../Helpers/messages';
import { showLoading } from '../Helpers/loading';
import isEmpty from 'validator/lib/isEmpty';

//  Redux

import { useSelector, useDispatch } from 'react-redux';
import { clearMessages } from '../../Redux/actions/messageActions';
import { createProduct } from '../../Redux/actions/productActions';

const AdminProductModal = () => {
   /*****************************
    *
    *  REDUX GLOBAL STATE PROPERTIES
    *
    *  ************************/

   const { loading } = useSelector((state) => state.loading);
   const { successMsg, errorMsg } = useSelector((state) => state.messages);
   const { categories } = useSelector((state) => state.categories);
   const dispatch = useDispatch();

   /*****************************
    *
    *  COMPONENT STATE PROPERTIES
    *
    *  ************************/
   const [clientSideErrorMsg, setClientSIdeErrorMsg] = useState('');
   const [productData, setProductData] = useState({
      productImage: null,
      productName: '',
      productDesc: '',
      productPrice: '',
      productCategory: '',
      productQty: '',
   });

   const {
      productImage,
      productName,
      productDesc,
      productPrice,
      productCategory,
      productQty,
   } = productData;

   /* LIFECYCLE METHODS*/

   /** EVENT  HANDLERS  */
   const handleMessage = () => {
      dispatch(clearMessages());
   };
   const handleProductChange = (e) => {
      dispatch(clearMessages());
      setProductData({
         ...productData,
         [e.target.name]: e.target.value,
      });
   };

   const handleProductImage = (e) => {
      dispatch(clearMessages());
      setProductData({
         ...productData,
         [e.target.name]: e.target.files[0],
      });
   };

   const handleProductSubmit = (e) => {
      e.preventDefault();
      if (productImage === null) {
         setClientSIdeErrorMsg('Please select an image');
      } else if (
         isEmpty(productName) ||
         isEmpty(productDesc) ||
         isEmpty(productPrice)
      ) {
         setClientSIdeErrorMsg('All fields are required');
      } else if (isEmpty(productCategory)) {
         setClientSIdeErrorMsg('PLease select a category');
      } else if (isEmpty(productQty)) {
         setClientSIdeErrorMsg('Please Enter Quantity');
      } else {
         showLoading(loading);
         let formData = new FormData();

         formData.append('productImage', productImage);
         formData.append('productName', productName);
         formData.append('productDesc', productDesc);
         formData.append('productPrice', productPrice);
         formData.append('productCategory', productCategory);
         formData.append('productQty', productQty);

         dispatch(createProduct(formData));
         setProductData({
            productImage: null,
            productName: '',
            productDesc: '',
            productPrice: '',
            productCategory: '',
            productQty: '',
         });
      }
   };
   return (
      /**  VIEWS  **/
      <div id='addProductModal' className='modal' onClick={handleMessage}>
         <div className='modal-dialog modal-dialog-centered modal-lg'>
            <div className='modal-content'>
               <form
                  onSubmit={handleProductSubmit}
                  encType='multipart/form-data'
               >
                  <div className='modal-header bg-secondary text-white'>
                     <h5 className='modal-title'>Add Product</h5>
                     <button className='close' data-dismiss='modal'>
                        <i className='fas fa-times'></i>
                     </button>
                  </div>
                  <div className='modal-body my-2'>
                     {clientSideErrorMsg && showErrMsg(clientSideErrorMsg)}
                     {errorMsg && showErrMsg(errorMsg)}
                     {successMsg && showSuccessMsg(successMsg)}
                     {loading ? (
                        <div className='text-center'>
                           {showLoading(loading)}
                        </div>
                     ) : (
                        <Fragment>
                           <div className='custom-file mb-2'>
                              <input
                                 type='file'
                                 className='custom-file-input'
                                 name='productImage'
                                 id='productImage'
                                 onChange={handleProductImage}
                              />
                              <label className='custom-file-label'>
                                 Choose file
                              </label>
                           </div>
                           <div className='form-group'>
                              <label className='text-secondary'> Name</label>
                              <input
                                 type='text'
                                 className='form-control'
                                 name='productName'
                                 value={productName}
                                 onChange={handleProductChange}
                              />
                           </div>
                           <div className='form-group'>
                              <label className='text-secondary'>
                                 Description
                              </label>
                              <textarea
                                 className='form-control'
                                 rows='3'
                                 name='productDesc'
                                 value={productDesc}
                                 onChange={handleProductChange}
                              ></textarea>
                           </div>
                           <div className='form-group'>
                              <label className='text-secondary'> Price</label>
                              <input
                                 type='text'
                                 className='form-control'
                                 name='productPrice'
                                 value={productPrice}
                                 onChange={handleProductChange}
                              />
                           </div>
                           <div className='form-row'>
                              <div className='col-md-6'>
                                 <label className='text-secondary'>
                                    Choose Category
                                 </label>
                                 <select
                                    className='custom-select mr-sm-2'
                                    name='productCategory'
                                    onChange={handleProductChange}
                                 >
                                    <option value={productCategory}>
                                       Choose
                                    </option>
                                    {categories &&
                                       categories.map((c) => (
                                          <option key={c._id} value={c._id}>
                                             {c.category}
                                          </option>
                                       ))}
                                 </select>
                              </div>
                              <div className='col-md-6'>
                                 <label className='text-secondary'>
                                    Quantity
                                 </label>
                                 <input
                                    type='number'
                                    className='form-control'
                                    min='0'
                                    name='productQty'
                                    value={productQty}
                                    onChange={handleProductChange}
                                 />
                              </div>
                           </div>
                        </Fragment>
                     )}
                  </div>
                  <div className='modal-footer'>
                     <button data-dismiss='modal' className='btn btn-secondary'>
                        Close
                     </button>
                     <button type='submit' className='btn btn-primary'>
                        Submit
                     </button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   );
};

export default AdminProductModal;
