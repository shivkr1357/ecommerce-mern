import React from 'react';

const AdminActionBtns = () => (
   <div className='bg-light'>
      <div className='container'>
         <div className='row pb-2 mb-1'>
            <div className='col-md-2 my-1'>
               <button
                  className='btn btn-outline-info btn-block'
                  data-toggle='modal'
                  data-target='#addCategoryModal'
               >
                  <i className='fas fa-plus'></i> Category
               </button>
            </div>
            <div className='col-md-2 my-1'>
               <button className='btn btn-outline-dark btn-block'>
                  <i className='fas fa-truck'></i> Brands
               </button>
            </div>
            <div className='col-md-2 my-1'>
               <button className='btn btn-outline-primary btn-block'>
                  <i className='fas fa-money-check-alt'></i> Orders
               </button>
            </div>
            <div className='col-md-2 my-1'>
               <button className='btn btn-outline-warning btn-block'>
                  <i className='fas fa-user'></i> Users
               </button>
            </div>
            <div className='col-md-2 my-1'>
               <button
                  className='btn btn-outline-secondary btn-block'
                  data-toggle='modal'
                  data-target='#addProductModal'
               >
                  <i className='fas fa-cart-arrow-down'></i> Products
               </button>
            </div>
            <div className='col-md-2 my-1'>
               <button className='btn btn-outline-success btn-block'>
                  <i className='fas fa-wrench'></i> Setting
               </button>
            </div>
         </div>
      </div>
   </div>
);

export default AdminActionBtns;
