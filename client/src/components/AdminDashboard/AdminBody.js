import React from 'react';
import Card from '../Card/Card';
//Redux

import { useSelector } from 'react-redux';

/**
 * @author
 * @function AdminBody
 **/

const AdminBody = (props) => {
   const { products } = useSelector((state) => state.products);
   console.log(products);
   return (
      <div className='container'>
         <div className='row'>
            <div className='card-deck'>
               {products.map((product) => (
                  <Card key={product._id} product={product} />
               ))}
            </div>
         </div>
      </div>
   );
};

export default AdminBody;
