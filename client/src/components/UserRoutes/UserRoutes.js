import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isAuthinticated } from '../Helpers/auth';

const UserRoutes = ({ component: Component, ...rest }) => {

    return(
        <Route
            {...rest}
            render = {(props) => 
                isAuthinticated() && isAuthinticated().role === 0 ? (
                    <Component {...props} />
                ):(
                    <Redirect to="/signIn" />
                )
            }
        />
    )

}

export default UserRoutes;