const express = require('express');
const router = express.Router();
const productsController = require('../controllers/products');
const { authenticateJWT } = require('../middleware/authenticator');
const upload = require('../middleware/multer');

router.post(
   '/',
   authenticateJWT,
   upload.single('productImage'),
   productsController.create
);
router.get('/', productsController.readAll);

module.exports = router;
