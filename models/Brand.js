const mongoose = require('mongoose');

const brandSchema = mongoose.Schema(
   {
      brand: {
         type: String,
         required: true,
      },
      brand_image: {
         type: String,
         required: true,
      },
   },
   { timestamps: true }
);

const Brand = new mongoose.model('Brand', brandschema);

module.exports = Brand;
