const mongoose = require('mongoose');

const categorySchema = mongoose.Schema(
   {
      category: {
         type: String,
         required: true,
         trim: true,
         maxlength: 50,
      },
   },
   { timestamps: true }
);

const Category = new mongoose.model('Category', categorySchema);

module.exports = Category;
