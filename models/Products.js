const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema;

const productSchema = mongoose.Schema(
   {
      fileName: {
         type: String,
         required: true,
      },
      productName: {
         type: String,
         required: true,
         trim: true,
         maxlength: 50,
      },
      productDesc: {
         type: String,
         trim: true,
      },
      productPrice: {
         type: Number,
         required: true,
      },
      productCategory: {
         type: ObjectId,
         ref: 'Category',
         required: true,
      },
      productQty: {
         type: Number,
         required: true,
      },
   },
   { timestamps: true }
);

const Products = new mongoose.model('Product', productSchema);

module.exports = Products;
