const Product = require('../models/Products');

exports.create = async (req, res) => {
   const { filename } = req.file;
   const {
      productName,
      productDesc,
      productCategory,
      productQty,
      productPrice,
   } = req.body;

   try {
      let product = new Product();
      product.fileName = filename;
      product.productName = productName;
      product.productDesc = productDesc;
      product.productCategory = productCategory;
      product.productPrice = productPrice;
      product.productQty = productQty;

      await product.save();
      res.json({
         successMessage: productName + ' was created',
         product,
      });
   } catch (err) {
      console.log('ProductController.create Error', err);
      res.status(500).json({
         errorMessage: 'Please Try again Later',
      });
   }
};

exports.readAll = async (req, res) => {
   try {
      const products = await Product.find({}).populate(
         'productCategory',
         'category'
      );

      res.json({ products });
   } catch (err) {
      console.log('ProductController.readAll Error', err);
      res.status(500).json({
         errorMessage: 'Please Try again Later',
      });
   }
};
