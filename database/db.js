const mongoose = require('mongoose');

const connectDB = async () => {
   try {
      await mongoose.connect(
         'mongodb+srv://pet-food-user:testing123@pet-food-database.gkxva.mongodb.net/<dbname>?retryWrites=true&w=majority',
         {
            useNewUrlParser: true,
            useUnifiedTopology: true,
         }
      );

      console.log('Database Connected Successfully');
   } catch (err) {
      console.log(err);
   }
};

module.exports = connectDB;
